CREATE DATABASE [EstateAgency]
GO
USE [EstateAgency]
GO
/****** Object:  Table [dbo].[EstateObject]    Script Date: 29.05.2022 12:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstateObject](
	[Estet_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Estet_Type] [nvarchar](40) NOT NULL,
	[Street] [nvarchar](60) NOT NULL,
	[Estet_Square] [float] NOT NULL,
	[Estet_Floor] [int] NULL,
	[Rooms] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Photo] [nvarchar](max) NOT NULL,
	[Realtor_ID] [int] NOT NULL,
 CONSTRAINT [PK__EstateOb__4F0EB5F7BE7B333C] PRIMARY KEY CLUSTERED 
(
	[Estet_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Realtor]    Script Date: 29.05.2022 12:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Realtor](
	[Realtor_ID] [int] IDENTITY(1,1) NOT NULL,
	[Phone] [nvarchar](12) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[SurName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__Realtor__CA91915F0819531E] PRIMARY KEY CLUSTERED 
(
	[Realtor_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EstateObject]  WITH CHECK ADD  CONSTRAINT [FK__EstateObj__Realt__267ABA7A] FOREIGN KEY([Realtor_ID])
REFERENCES [dbo].[Realtor] ([Realtor_ID])
GO
ALTER TABLE [dbo].[EstateObject] CHECK CONSTRAINT [FK__EstateObj__Realt__267ABA7A]
GO
