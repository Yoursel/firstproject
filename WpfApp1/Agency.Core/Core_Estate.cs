﻿namespace Agency.Core
{
    public class Estate_Objects
    {
        ulong estate_id;
        ulong realtor_id;
        string type_of_estate;  // тип имущес
        string adress;          // адресс
        float living_space;     // жилая площадь
        int floor;              // этаж
        uint room_quantity;     // количество комнат
        decimal price;          // цена
        string photo;           // ссылка на фото
    }

    public class Realtor
    {
        ulong id;
        string first_name;  // имя
        string sur_name;    // фамилия
        string middle_name; // отчество
        string phone_number;
    }
}